/*
Navicat MySQL Data Transfer

Source Server         : db_local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : db_compro_ceklis

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2022-05-25 15:26:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_article_id` int(11) DEFAULT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penulis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1', '2', 'Equity Crowdfunding And Its Potential In Indonesian Market', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'admin', null, '1653231190.png', 'equity-crowdfunding-and-its-potential-in-indonesian-market', '1', null, null, '2022-05-22 14:53:10', '2022-05-24 06:53:28');
INSERT INTO `article` VALUES ('2', '2', 'Alur Fortuna', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'tamis', null, '1653231440.jpg', 'alur-fortuna', '1', null, null, '2022-05-22 14:57:20', '2022-05-24 06:53:12');

-- ----------------------------
-- Table structure for category_article
-- ----------------------------
DROP TABLE IF EXISTS `category_article`;
CREATE TABLE `category_article` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of category_article
-- ----------------------------
INSERT INTO `category_article` VALUES ('2', 'Kesehatan', 'kesehatan', '1', null, null, '2022-05-22 14:47:28', '2022-05-22 14:47:28');
INSERT INTO `category_article` VALUES ('3', 'Olahragas', 'olahragas', '1', null, null, '2022-05-22 14:47:51', '2022-05-22 14:51:09');

-- ----------------------------
-- Table structure for content
-- ----------------------------
DROP TABLE IF EXISTS `content`;
CREATE TABLE `content` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_content` int(11) NOT NULL COMMENT '1 => Slider, 2 => Home Banner, 3 => Top Banner, 4 => Accounting Services',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of content
-- ----------------------------
INSERT INTO `content` VALUES ('3', '2', null, null, '1', null, '1653226473.png', '', null, null, null, null, null, null, null, '2022-05-22 13:34:33', '2022-05-22 13:35:47');
INSERT INTO `content` VALUES ('4', '3', null, null, '1', null, '1653226658.jpg', '', null, null, null, null, null, null, null, '2022-05-22 13:37:38', '2022-05-22 13:37:38');
INSERT INTO `content` VALUES ('7', '1', null, '<h3><strong>Ceklis impian anda&nbsp;<br />\r\ndalam membangun bisnis</strong></h3>\r\n\r\n<p>Kami di sini hadir untuk membantu bisnis Anda</p>\r\n\r\n<p>untuk berkolaborasi menciptakan strategi bersama.</p>', '1', 'http://', '1653290317.png', '', null, null, null, null, null, null, null, '2022-05-23 07:18:37', '2022-05-23 08:35:54');
INSERT INTO `content` VALUES ('8', '4', 'Penghasilan Bruto  Rp. 0jt s.d Rp. 50jt', '<ul>\r\n	<li>Neraca</li>\r\n	<li>Laba rugi</li>\r\n	<li>Arus Kas</li>\r\n	<li>Report Tahunan</li>\r\n</ul>', '1', null, '1653385849.png', 'penghasilan-bruto-rp-0jt-sd-rp-50jt', null, null, null, null, null, null, null, '2022-05-24 09:50:49', '2022-05-24 09:50:49');
INSERT INTO `content` VALUES ('9', '4', 'Penghasilan Bruto  Rp. 200jt s.d Rp. 500jt', '<ul>\r\n	<li>Neraca</li>\r\n	<li>Laba rugi</li>\r\n	<li>Arus Kas</li>\r\n	<li>Report Tahunan&nbsp;</li>\r\n	<li>Tatap muka max. 2x/bln</li>\r\n</ul>\r\n\r\n<div id=\"gtx-trans\" style=\"position: absolute; left: -41px; top: -4.8px;\">\r\n<div class=\"gtx-trans-icon\">&nbsp;</div>\r\n</div>', '1', null, '1653385892.png', 'penghasilan-bruto-rp-200jt-sd-rp-500jt', null, null, null, null, null, null, null, '2022-05-24 09:51:32', '2022-05-24 09:51:32');
INSERT INTO `content` VALUES ('10', '4', 'Penghasilan Bruto  Rp. 50jt s.d Rp. 200jt', '<ul>\r\n	<li>Neraca</li>\r\n	<li>Laba rugi</li>\r\n	<li>Arus Kas</li>\r\n	<li>Report Tahunan</li>\r\n	<li>Tatap muka max. 1x/bln</li>\r\n</ul>\r\n\r\n<div id=\"gtx-trans\" style=\"position: absolute; left: -31px; top: -4.8px;\">\r\n<div class=\"gtx-trans-icon\">&nbsp;</div>\r\n</div>', '1', null, '1653386696.png', 'penghasilan-bruto-rp-50jt-sd-rp-200jt', null, null, null, null, null, null, null, '2022-05-24 10:04:56', '2022-05-24 10:04:56');
INSERT INTO `content` VALUES ('11', '4', 'Penghasilan Bruto  Rp. 500jt s.d Rp. 1M', '<ul>\r\n	<li>Neraca</li>\r\n	<li>Laba rugi</li>\r\n	<li>Arus Kas</li>\r\n	<li>Report Tahunan</li>\r\n	<li>Tatap muka max. 3x/bln</li>\r\n</ul>\r\n\r\n<div id=\"gtx-trans\" style=\"position: absolute; left: -53px; top: -4.8px;\">\r\n<div class=\"gtx-trans-icon\">&nbsp;</div>\r\n</div>', '1', null, '1653386736.png', 'penghasilan-bruto-rp-500jt-sd-rp-1m', null, null, null, null, null, null, null, '2022-05-24 10:05:36', '2022-05-24 10:05:36');
INSERT INTO `content` VALUES ('12', '4', 'Penghasilan Bruto  > 1M', '<ul>\r\n	<li>Neraca</li>\r\n	<li>Laba rugi</li>\r\n	<li>Arus Kas</li>\r\n	<li>Report Tahunan&nbsp;</li>\r\n	<li>Tatap muka max. 4x/bln</li>\r\n</ul>\r\n\r\n<div id=\"gtx-trans\" style=\"position: absolute; left: -15px; top: -4.8px;\">\r\n<div class=\"gtx-trans-icon\">&nbsp;</div>\r\n</div>', '1', null, '1653386810.png', 'penghasilan-bruto-1m', null, null, null, null, null, null, null, '2022-05-24 10:06:50', '2022-05-24 10:06:50');
INSERT INTO `content` VALUES ('13', '5', 'Tax Compiliance', '<ul>\r\n	<li>PPh Orang Pribadi (Tahunan)</li>\r\n	<li>PPh Bulanan (Ps.4 (2), Ps.21,</li>\r\n	<li>Ps.23)</li>\r\n	<li>PPh Tahunan Badan</li>\r\n	<li>PPN</li>\r\n</ul>', '1', null, '1653387468.png', 'tax-compiliance', null, null, null, null, null, null, null, '2022-05-24 10:17:48', '2022-05-24 10:17:48');
INSERT INTO `content` VALUES ('14', '5', 'Dispute & Resolution', '<ul>\r\n	<li>Pemeriksaan</li>\r\n	<li>Keberatan</li>\r\n	<li>Banding</li>\r\n</ul>\r\n\r\n<div id=\"gtx-trans\" style=\"position: absolute; left: 0px; top: -4.8px;\">\r\n<div class=\"gtx-trans-icon\">&nbsp;</div>\r\n</div>', '1', null, '1653387523.png', 'dispute-resolution', null, null, null, null, null, null, null, '2022-05-24 10:18:43', '2022-05-24 10:19:58');
INSERT INTO `content` VALUES ('15', '5', 'Tax Review & Consultation', '<ul>\r\n	<li>Tax Review/Due Diligence</li>\r\n	<li>Consultation</li>\r\n	<li>Tax Planning</li>\r\n</ul>\r\n\r\n<div id=\"gtx-trans\" style=\"position: absolute; left: -40px; top: -4.8px;\">\r\n<div class=\"gtx-trans-icon\">&nbsp;</div>\r\n</div>', '1', null, '1653387556.png', 'tax-review-consultation', null, null, null, null, null, null, null, '2022-05-24 10:19:16', '2022-05-24 10:19:16');
INSERT INTO `content` VALUES ('16', '6', 'Financial Strategy', '<p>Financial Strategy is often the most overlooked aspect of a budding company growt. We can help you with that.</p>', '1', null, null, 'financial-strategy', null, null, null, null, null, null, null, '2022-05-24 10:28:49', '2022-05-24 10:28:49');
INSERT INTO `content` VALUES ('17', '6', 'Valuation Setting', '<p>Growing the business and execution is key. Leave the fund raising valution and financial model to the experts.</p>', '1', null, null, 'valuation-setting', null, null, null, null, null, null, null, '2022-05-24 10:29:25', '2022-05-24 10:29:25');
INSERT INTO `content` VALUES ('18', '6', 'Fund-Raising Perp', '<p>We speak the investor lenguage. Let us help you with your pict deck and mock pitch day presentation.</p>', '1', null, null, 'fund-raising-perp', null, null, null, null, null, null, null, '2022-05-24 10:30:03', '2022-05-24 10:30:03');
INSERT INTO `content` VALUES ('19', '6', 'Business Model Review', '<p>Trough financial models, we review your business model&rsquo;s viabillity and robustness.</p>', '1', null, null, 'business-model-review', null, null, null, null, null, null, null, '2022-05-24 10:30:35', '2022-05-24 10:30:35');
INSERT INTO `content` VALUES ('21', '7', 'Pendirian CV', '{\"IDR\":{\"idr_lite\":\"4000000\",\"idr_full\":\"6000000\",\"idr_plus\":\"8500000\"},\"konsultasi_gratis\":{\"konsultasi_gratis_lite\":\"YA\",\"konsultasi_gratis_full\":\"YA\",\"konsultasi_gratis_plus\":\"YA\"},\"akta_perusahaan\":{\"akta_perusahaan_lite\":\"YA\",\"akta_perusahaan_full\":\"YA\",\"akta_perusahaan_plus\":\"YA\"},\"sk_menkuham\":{\"sk_menkuham_lite\":\"TIDAK\",\"sk_menkuham_full\":\"YA\",\"sk_menkuham_plus\":\"YA\"},\"npwp_skt\":{\"npwp_skt_lite\":\"TIDAK\",\"npwp_skt_full\":\"YA\",\"npwp_skt_plus\":\"YA\"},\"siup\":{\"siup_lite\":\"TIDAK\",\"siup_full\":\"YA\",\"siup_plus\":\"YA\"},\"nib_tdp_api\":{\"nib_tdp_api_lite\":\"TIDAK\",\"nib_tdp_api_full\":\"YA\",\"nib_tdp_api_plus\":\"YA\"},\"virtual_office\":{\"virtual_office_lite\":\"TIDAK\",\"virtual_office_full\":\"TIDAK\",\"virtual_office_plus\":\"YA\"}}', '1', null, null, 'pendirian-cv', null, null, null, null, null, null, null, '2022-05-25 02:22:51', '2022-05-25 02:49:27');
INSERT INTO `content` VALUES ('22', '7', 'Pendirian PT', '{\"IDR\":{\"idr_lite\":\"5000000\",\"idr_full\":\"8500000\",\"idr_plus\":\"10500000\"},\"konsultasi_gratis\":{\"konsultasi_gratis_lite\":\"YA\",\"konsultasi_gratis_full\":\"YA\",\"konsultasi_gratis_plus\":\"YA\"},\"akta_perusahaan\":{\"akta_perusahaan_lite\":\"YA\",\"akta_perusahaan_full\":\"YA\",\"akta_perusahaan_plus\":\"YA\"},\"sk_menkuham\":{\"sk_menkuham_lite\":\"YA\",\"sk_menkuham_full\":\"YA\",\"sk_menkuham_plus\":\"YA\"},\"npwp_skt\":{\"npwp_skt_lite\":\"TIDAK\",\"npwp_skt_full\":\"YA\",\"npwp_skt_plus\":\"YA\"},\"siup\":{\"siup_lite\":\"TIDAK\",\"siup_full\":\"YA\",\"siup_plus\":\"YA\"},\"nib_tdp_api\":{\"nib_tdp_api_lite\":\"TIDAK\",\"nib_tdp_api_full\":\"YA\",\"nib_tdp_api_plus\":\"YA\"},\"virtual_office\":{\"virtual_office_lite\":\"TIDAK\",\"virtual_office_full\":\"TIDAK\",\"virtual_office_plus\":\"YA\"}}', '1', null, null, 'pendirian-pt', null, null, null, null, null, null, null, '2022-05-25 02:51:10', '2022-05-25 02:51:10');
INSERT INTO `content` VALUES ('23', '7', 'Pendirian PT PMA', '{\"IDR\":{\"idr_lite\":\"10000000\",\"idr_full\":\"20000000\",\"idr_plus\":\"25000000\"},\"konsultasi_gratis\":{\"konsultasi_gratis_lite\":\"YA\",\"konsultasi_gratis_full\":\"YA\",\"konsultasi_gratis_plus\":\"YA\"},\"akta_perusahaan\":{\"akta_perusahaan_lite\":\"YA\",\"akta_perusahaan_full\":\"YA\",\"akta_perusahaan_plus\":\"YA\"},\"sk_menkuham\":{\"sk_menkuham_lite\":\"YA\",\"sk_menkuham_full\":\"YA\",\"sk_menkuham_plus\":\"YA\"},\"npwp_skt\":{\"npwp_skt_lite\":\"TIDAK\",\"npwp_skt_full\":\"YA\",\"npwp_skt_plus\":\"YA\"},\"siup\":{\"siup_lite\":\"TIDAK\",\"siup_full\":\"YA\",\"siup_plus\":\"YA\"},\"nib_tdp_api\":{\"nib_tdp_api_lite\":\"TIDAK\",\"nib_tdp_api_full\":\"YA\",\"nib_tdp_api_plus\":\"YA\"},\"virtual_office\":{\"virtual_office_lite\":\"TIDAK\",\"virtual_office_full\":\"TIDAK\",\"virtual_office_plus\":\"YA\"}}', '1', null, null, 'pendirian-pt-pma', null, null, null, null, null, null, null, '2022-05-25 02:52:03', '2022-05-25 02:52:03');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for information_content
-- ----------------------------
DROP TABLE IF EXISTS `information_content`;
CREATE TABLE `information_content` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_content` int(11) NOT NULL COMMENT '1 => About Us, 2 => Contact, 3 => Social Media, 4 => FAQ',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `link` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of information_content
-- ----------------------------
INSERT INTO `information_content` VALUES ('2', '1', 'ABC', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>&nbsp;</p>', '1', null, '1653358327.png', 'abc', '<p><strong>Lorem Ipsum</strong></p>', null, null, null, null, null, null, '2022-05-21 02:31:38', '2022-05-24 04:34:27');
INSERT INTO `information_content` VALUES ('4', '2', 'CONTACT CEKLIS', '<p>Ciputra International<br />\r\nTokopedia Care Tower<br />\r\nFoor 15 Unit 05<br />\r\nJl. Lingkar Luar Barat No. 101<br />\r\nJakarta 11740</p>', '1', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15864.534612796362!2d106.8262341!3d-6.2461123!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x33234bffa6d6c497!2sPT%20Kreasi%20Terdepan%20Indonesia!5e0!3m2!1sen!2sid!4v1641915931547!5m2!1sen!2sid', null, 'contact-ceklis', '+62217989323', '+6287780207193', null, 'info@ceklis.com', null, null, null, '2022-05-22 06:18:57', '2022-05-24 01:34:40');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2019_08_19_000000_create_failed_jobs_table', '1');
INSERT INTO `migrations` VALUES ('4', '2019_12_14_000001_create_personal_access_tokens_table', '1');
INSERT INTO `migrations` VALUES ('5', '2022_05_19_080040_create_content', '2');
INSERT INTO `migrations` VALUES ('6', '2022_05_19_080056_create_information_content', '2');
INSERT INTO `migrations` VALUES ('7', '2022_05_19_080108_create_team', '2');
INSERT INTO `migrations` VALUES ('8', '2022_05_19_080148_create_questions', '2');
INSERT INTO `migrations` VALUES ('9', '2022_05_19_082711_create_article', '2');
INSERT INTO `migrations` VALUES ('10', '2022_05_19_083159_create_category_article', '2');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for questions
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama_depan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_belakang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_hp` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pesan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `industri` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pesan_balasan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of questions
-- ----------------------------
INSERT INTO `questions` VALUES ('1', 'Afifur', 'Rahman', 'afifurrahman1127@gmail.com', '+6282133330227', 'sddasd', null, null, null, null, null, '2022-05-24 08:26:59', '2022-05-24 08:26:59');

-- ----------------------------
-- Table structure for team
-- ----------------------------
DROP TABLE IF EXISTS `team`;
CREATE TABLE `team` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_hp1` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_hp2` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_fb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_ig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_google` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of team
-- ----------------------------
INSERT INTO `team` VALUES ('2', 'Fathus Selmi', 'Managing Partner', 'fathus.selmi@prodana.id', '+6282133330227', null, 'Gn tamtama 1', 'fathus-selmi', '1653357963.png', 'sdda', null, null, null, null, null, null, null, '2022-05-22 03:51:13', '2022-05-24 02:06:03');
INSERT INTO `team` VALUES ('4', 'Yandhi Surya', 'Partner-Corporate Finanace', 'afifurrahman1127@gmail.com', '+6282133330227', null, 'Gn tamtama 1', 'yandhi-surya', '1653358017.png', null, null, null, null, null, null, null, null, '2022-05-24 02:06:57', '2022-05-24 02:06:57');
INSERT INTO `team` VALUES ('5', 'Garry Aanggi Butar-Butar', 'Partner Tax', 'afifurrahman1127@gmail.com', '+6282133330227', null, 'Gn tamtama 1', 'garry-aanggi-butar-butar', '1653358072.png', null, null, null, null, null, null, null, null, '2022-05-24 02:07:52', '2022-05-24 02:07:52');
INSERT INTO `team` VALUES ('6', 'Mardi Abas Barus', 'Partner Accounting and Audit', 'afifurrahman1127@gmail.com', '+6282133330227', null, 'Gn tamtama 1', 'mardi-abas-barus', '1653358128.png', null, null, null, null, null, null, null, null, '2022-05-24 02:08:48', '2022-05-24 02:08:48');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('2', 'Afifur Rahman', 'afifurrahman27@gmail.com', null, '$2y$10$dNcYQJ7iNEMAAowxIWCkgek0yD6aLw4091LsvoQ0fQUSPVsKzQEJy', null, '2022-05-20 10:37:39', '2022-05-22 06:40:18');
