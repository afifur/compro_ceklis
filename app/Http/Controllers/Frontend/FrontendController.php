<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;
use App\Models\ContentModel;
use App\Models\TeamModel;
use App\Models\InformationModel;
use App\Models\ArtikelModel;
use App\Models\KategoriArtikelModel;
use App\Models\QuestionsModel;
use App\Models\TestimonialModel;

class FrontendController extends Controller
{
    public function home()
    {
    	$slider = ContentModel::where('type_content', 1)->where('status', 1)->get();
        $testimonial = TestimonialModel::where('status', 1)->get();
        $why_choose_us = ContentModel::where('type_content', 8)->where('status', 1)->get();
        $what_makes_us_uniq = ContentModel::where('type_content', 8)->where('status', 1)->get();

    	return view('frontend.home.home', compact('slider', 'testimonial', 'why_choose_us', 'what_makes_us_uniq'));
    }

    public function about_us()
    {
    	$about_us 	= InformationModel::where('type_content', 1)->first();
    	$team 		= TeamModel::where('status', 1)->get();
    	return view('frontend.about_us.about_us', compact('about_us', 'team'));
    }

    public function accounting_services()
    {
    	$accounting_services = ContentModel::where('type_content', 4)->where('status', 1)->get();
    	$contacts = InformationModel::where('type_content', 2)->first();

        return view('frontend.accounting_services.accounting_services', compact('accounting_services', 'contacts'));
    }

    public function tax_services()
    {
    	$tax_services = ContentModel::where('type_content', 5)->where('status', 1)->get();
        $contacts = InformationModel::where('type_content', 2)->first();

    	return view('frontend.tax_services.tax_services', compact('tax_services', 'contacts'));
    }

    public function finance_services()
    {
    	$finance_services = ContentModel::where('type_content', 6)->where('status', 1)->get();
    	return view('frontend.finance_services.finance_services', compact('finance_services'));
    }

    public function pendirian_badan_usaha()
    {
    	$pendirian_badan_usaha = ContentModel::where('type_content', 7)->where('status', 1)->get();
    	return view('frontend.pendirian_badan_usaha.pendirian_badan_usaha', compact('pendirian_badan_usaha'));
    }

    public function article()
    {
    	$article = ArtikelModel::where('status', 1)->orderBy('created_at', 'DESC')->paginate(9);
    	return view('frontend.article.article', compact('article'));
    }

    public function detail_article($slug)
    {
    	$detail = ArtikelModel::where('slug', $slug)->where('status', 1)->first();
    	$other_article = ArtikelModel::where('slug','!=', $slug)->where('status', 1)->orderBy('created_at', 'DESC')->take(3)->get();
    	return view('frontend.article.detail_article', compact('detail', 'other_article'));
    }

    public function contacts()
    {
    	$contacts = InformationModel::where('type_content', 2)->first();
    	return view('frontend.contacts.contacts', compact('contacts'));
    }

    public function privacy()
    {
        $privacy = InformationModel::where('type_content', 4)->where('status', 1)->first();
        $term = InformationModel::where('type_content', 5)->where('status', 1)->first();

        return view('frontend.privacy.privacy', compact('privacy', 'term'));
    }

    public function term()
    {
        $privacy = InformationModel::where('type_content', 4)->where('status', 1)->first();
        $term = InformationModel::where('type_content', 5)->where('status', 1)->first();

        return view('frontend.term.term', compact('privacy','term'));
    }

    public function save_contacts(Request $request)
    {
    	try {
    		$create = new QuestionsModel;
    		$create->nama_depan = !empty($request->input('nama_depan')) ? $request->input('nama_depan') : NULL;
    		$create->nama_belakang = !empty($request->input('nama_belakang')) ? $request->input('nama_belakang') : NULL;
    		$create->email = !empty($request->input('email')) ? $request->input('email') : NULL;
    		$create->no_hp = !empty($request->input('no_hp')) ? $request->input('no_hp') : NULL;
    		$create->pesan = !empty($request->input('pesan')) ? $request->input('pesan') : NULL;
    		
    		if ($create->save()) {
		  		Alert::success('Success', 'Pesan anda telah terkirim ke Ceklis.id');
		  	}else{
		  		Alert::error('Error', 'Gagal');
		  	}

    	} catch (\Exception $e) {
    		Alert::error('Error', $e->getMessage());
    	}

    	return redirect(route('contacts'));
    }
}
