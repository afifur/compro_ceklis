<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InformationModel extends Model
{
    use HasFactory;

    protected $table = 'information_content';

    protected $fillable = [
    	"id",
    	"type_content",
        "title",
    	"description",
    	"status",
    	"link",
    	"image",
    	"slug",
    	"value1",
    	"value2",
    	"value3",
    	"value4",
    	"value5",
    ];
}
