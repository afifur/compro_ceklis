@php
    use App\Models\InformationModel;
    $uri = Request::segment(1);
    $social_media = InformationModel::where('type_content', 3)->get();
    $contact = InformationModel::where('type_content', 2)->first();
    $privacy = InformationModel::where('type_content', 4)->where('status', 1)->first();
    $term = InformationModel::where('type_content', 5)->where('status', 1)->first();

    $stickyNav = "";
    if($uri != NULL){
        $stickyNav = "header-sticky sticky-active";
    }
@endphp

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Ceklis') }}</title>

    <!-- Scripts -->
    <link href="{{ url('frontend/css/plugins.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('frontend/css/style.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .custom-max-width{
            max-width: 19.666667%;
        }

        .container-fluid {
            padding-left: 5% !important;
            padding-right: 5% !important;
        }

        .custom-title-service {
            font-size: 10px;
            display: block;
        }

        @media only screen and (max-device-width: 480px) {
            .custom-title-service {
                display: none;
            }
        }

        .menu-footer{
            padding: 20px; 
            position: fixed; 
            left: 0; 
            bottom: 0; 
            width: 100%;
            background-color: #FFFFFF;
            z-index: 999;
            box-shadow: 1px 2px 4px 0px;
        }

        .custom-widget-title {
            font-style: normal !important;
            font-weight: 350 !important;
            font-size: 13px !important;
            color: #0F1014 !important;
        }

        .custom-list li a{
            font-size: 12px !important;
            color: #4A5063 !important;
            font-style: normal !important;
            font-weight: 325 !important;
        }

        .custom-list-menu-footer li {
            height: 25px !important;
        }

        .custom-list-menu-footer li a{
            font-size: 12px !important;
            color: #FB6E16 !important;
            font-style: normal !important;
            font-weight: 325 !important;
        }

        .custom-footer-size {
            font-size: 12px !important;
            color: #4A5063 !important;
            font-style: normal !important;
            font-weight: 325 !important;
        }

        .footer-text{
            font-size: 12px !important;
            color: #4A5063 !important;
            font-style: normal !important;
            font-weight: 325 !important;
        }

        .privacy-text a{
            font-size: 12px !important;
            color: #4A5063 !important;
            font-style: normal !important;
            font-weight: 325 !important;
        }
    </style>
</head>
<body>
    <div class="body-inner">
        <header id="header" class="header-always-fixed {{ $stickyNav }}">
            <div class="header-inner">
                <div class="container-fluid">
                    <a href="{{ route('home-frontend') }}">
                        <div id="logo">
                            <img src="{{ url('frontend/images/logo.png') }}">
                        </div>
                    </a>
                    <div id="search"><a id="btn-search-close" class="btn-search-close" aria-label="Close search form"><i class="icon-x"></i></a>
                        <form class="search-form" action="search-results-page.html" method="get">
                            <input class="form-control" name="q" type="text" placeholder="Type & Search..." />
                            <span class="text-muted">Start typing & press "Enter" or "ESC" to close</span>
                        </form>
                    </div>
                    <div id="mainMenu-trigger">
                        <a class="lines-button x"><span class="lines"></span></a>
                    </div>
                    <div id="mainMenu">
                        <div class="container-fluid">
                            <nav>
                                <ul>
                                    <li><a href="{{ route('home-frontend') }}">HOME</a></li>
                                    <li><a href="{{ route('about-us') }}">ABOUT US</a></li>
                                    <li class="dropdown"><a href="#">SERVICES</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ route('accounting-service') }}">Accounting Services</a></li>
                                            <li><a href="{{ route('tax-service') }}">Tax Services</a></li>
                                            <li><a href="{{ route('finance-service') }}">Finance Services</a></li>
                                            <li><a href="{{ route('pendirian-badan-usaha') }}">Pendirian Badan Usaha</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{ route('article') }}">ARTICLE</a></li>
                                    <li><a href="{{ route('contacts') }}">CONTACTS</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>  
                </div>
            </div>
        </header>
        <div>
            @yield('content')
        </div>
        <footer id="footer">
            <div class="footer-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="widget">
                                <img src="{{ url('frontend/images/logo.png') }}">
                                <p class="custom-footer-size">Contact us by Social media</p>
                                <div class="social-icons social-icons-medium social-icons-rounded social-icons-colored">
                                    <ul>
                                        @if(count($social_media) > 0)
                                            @foreach($social_media as $social_medias)
                                            <li>
                                                <a href="{{ $social_medias->link }}" target="_blank">
                                                    <img src="{{ url('upload/social-media/'.$social_medias->image) }}">
                                                </a>
                                            </li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="widget">
                                        <div class="widget-title custom-widget-title">Contact Us</div>
                                        <span class="custom-footer-size">Ask how Ceklis.id can help you</span>
                                        <ul class="list custom-list">
                                            <li><i class="fa fa-phone"></i>&nbsp;&nbsp;<a href="#">{{ !empty($contact->value1) ? $contact->value1 : '' }}</a></li>
                                            <li><i class="fa fa-envelope"></i>&nbsp;&nbsp;<a href="#">{{ !empty($contact->value4) ? $contact->value4 : '' }}</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="widget">
                                        <div class="widget-title custom-widget-title">Location</div>
                                        <p class="custom-footer-size">
                                            {!! !empty($contact->description) ? html_entity_decode($contact->description) : '' !!}
                                        </p>
                                        <p class="custom-footer-size"><a href="{{ !empty($contact->link) ? $contact->link : '' }}" class="text-success" target="_blank"><i class="fa fa-map-marker"></i> Get direction</a></p>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="widget">
                                        <div class="widget-title custom-widget-title">Useful Links</div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <ul class="list custom-list-menu-footer">
                                                    <li><a href="{{ route('home-frontend') }}">Home</a></li>
                                                    <li><a href="{{ route('about-us') }}">About</a></li>
                                                    <li><a href="">Services</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-md-6">
                                                <ul class="list custom-list-menu-footer">
                                                    <li><a href="{{ route('article') }}">Article</a></li>
                                                    <li><a href="{{ route('contacts') }}">Contact</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright-content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="text-left footer-text">Copyright &copy; {!! date('Y') !!} Ceklis.id</div>
                    </div>
                    <div class="col-md-6">
                        <div class="text-right privacy-text">
                            @if(!empty($privacy->status) && $privacy->status == 1)
                            <a href="{{ route('privacy') }}">Privacy</a>&nbsp;&nbsp;&nbsp;
                            @endif

                            @if(!empty($term->status) && $term->status == 1)
                            <a href="{{ route('term') }}">Term of Us</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="menu-footer">
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="row">
                        <div class="col-md-3 custom-max-width">
                            <a href="{{ route('pendirian-badan-usaha') }}">
                                <div class="text-center">
                                    <img src="{{ url('frontend/images/services/1.png') }}" width="50" height="50">
                                    <h6 class="title-box-icon custom-title-service">Pendirian Usaha</h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 custom-max-width">
                            <a href="{{ route('accounting-service') }}">
                                <div class="text-center">
                                    <img src="{{ url('frontend/images/services/2.png') }}" width="50" height="50">
                                    <h6 class="title-box-icon custom-title-service">Accounting Services</h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 custom-max-width">
                            <a href="{{ route('tax-service') }}">
                                <div class="text-center">
                                    <img src="{{ url('frontend/images/services/3.png') }}" width="50" height="50">
                                    <h6 class="title-box-icon custom-title-service">Tax Services</h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 custom-max-width">
                            <div class="text-center">
                                <img src="{{ url('frontend/images/services/4.png') }}" width="50" height="50">
                                <h6 class="title-box-icon custom-title-service">Audit Services</h6>
                            </div>
                        </div>
                        <div class="col-md-3 custom-max-width">
                            <a href="{{ route('finance-service') }}">
                                <div class="text-center">
                                    <img src="{{ url('frontend/images/services/5.png') }}" width="50" height="50">
                                    <h6 class="title-box-icon custom-title-service">Finance Services</h6>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </footer>
        
    </div>
    <a id="scrollTop"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>

    @include('sweetalert::alert')
    <script src="{{ url('frontend/js/jquery.js') }}"></script>
    <script src="{{ url('frontend/js/plugins.js') }}"></script>
    <script src="{{ url('frontend/js/functions.js') }}"></script>
    @stack('scripts')
</body>
</html>