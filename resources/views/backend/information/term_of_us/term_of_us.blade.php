@extends('layouts.app_admin')

@section('content')
  <div class="row">
        <div class="col-md-8">
            <div class="block-web">
              <div class="header">
                <h3 class="content-header">Form Term Of Us</h3>
              </div>
              <div class="porlets-content">
                <form action="{{ route('save-term-of-us') }}" method="POST" enctype="multipart/form-data" class="form-horizontal row-border">
                  @csrf
                    <div class="form-group lable-padd">
                      <label class="col-sm-3">Title</label>
                      <div class="col-sm-9">
                        <input type="hidden" name="id" value="{{ !empty($term_of_us->id) ? $term_of_us->id : NULL }}">
                        <input required type="text" name="title" id="title" value="{{ !empty($term_of_us->title) ? $term_of_us->title : 'TERM OF US' }}" readonly class="form-control">
                      </div>
                    </div>
                    <div class="form-group lable-padd">
                      <label class="col-sm-3">Term of Us</label>
                      <div class="col-sm-9">
                        <textarea class="form-control my-editor" name="description" id="my-editor" rows="6">{{ !empty($term_of_us->description) ? $term_of_us->description : '' }}</textarea>
                      </div>
                    </div>
                    <div class="form-group lable-padd">
                      <label class="col-sm-3">Status</label>
                      <div class="col-sm-9">
                        <select required class="form-control" name="status" id="status">
                          <option value="">-- status --</option>
                          <option value="1" {{ !empty($term_of_us->status) && $term_of_us->status == 1 ? 'selected': '' }}>Aktif</option>
                          <option value="2" {{ !empty($term_of_us->status) && $term_of_us->status == 2 ? 'selected': '' }}>Non Aktif</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-success col-md-12">
                        Simpan
                      </button>
                    </div>
                </form>
              </div>
          </div>
      </div>
  </div>
@endsection

@push('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('my-editor');
    </script>
@endpush