@extends('layouts.app_admin')

@section('content')
	<div class="row">
        <div class="col-md-8">
          	<div class="block-web">
	            <div class="header">
	            	<h3 class="content-header">Form Privacy Police</h3>
	            </div>
	            <div class="porlets-content">
	            	<form action="{{ route('save-privacy-police') }}" method="POST" enctype="multipart/form-data" class="form-horizontal row-border">
	            		@csrf
              			<div class="form-group lable-padd">
              				<label class="col-sm-3">Title</label>
              				<div class="col-sm-9">
              					<input type="hidden" name="id" value="{{ !empty($privacy_police->id) ? $privacy_police->id : NULL }}">
              					<input required type="text" name="title" id="title" value="{{ !empty($privacy_police->title) ? $privacy_police->title : 'PRIVACY POLICE' }}" readonly class="form-control">
              				</div>
              			</div>
              			<div class="form-group lable-padd">
              				<label class="col-sm-3">Privacy</label>
              				<div class="col-sm-9">
              					<textarea class="form-control my-editor" name="description" id="my-editor" rows="6">{{ !empty($privacy_police->description) ? $privacy_police->description : '' }}</textarea>
              				</div>
              			</div>
                    <div class="form-group lable-padd">
                      <label class="col-sm-3">Status</label>
                      <div class="col-sm-9">
                        <select required class="form-control" name="status" id="status">
                          <option value="">-- status --</option>
                          <option value="1" {{ !empty($privacy_police->status) && $privacy_police->status == 1 ? 'selected': '' }}>Aktif</option>
                          <option value="2" {{ !empty($privacy_police->status) && $privacy_police->status == 2 ? 'selected': '' }}>Non Aktif</option>
                        </select>
                      </div>
                    </div>
              			<div class="form-group">
              				<button type="submit" class="btn btn-success col-md-12">
              					Simpan
              				</button>
              			</div>
	            	</form>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@push('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('my-editor');
    </script>
@endpush