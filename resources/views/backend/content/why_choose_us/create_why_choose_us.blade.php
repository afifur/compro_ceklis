@extends('layouts.app_admin')

@section('content')
<div class="row">
  <div class="col-md-10">
    <div class="block-web">
         <div class="header">
           <h3 class="content-header">
              <a href="{{ route('why-choose-us') }}"><i class="fa fa-arrow-left"></i></a> &nbsp;
              Form Why Choose Us
           </h3>
         </div>
         <div class="porlets-content">
            <form action="{{ route('save-why-choose-us') }}" method="POST" enctype="multipart/form-data" class="form-horizontal row-border">
                @csrf
                <div class="form-group lable-padd">
                  <label class="col-sm-3">Nama</label>
                  <div class="col-sm-9">
                     <input type="hidden" name="id" value="{{ !empty($why_choose_us->id) ? $why_choose_us->id : NULL }}">
                     <input required type="text" name="title" id="title" value="{{ !empty($why_choose_us->title) ? $why_choose_us->title : '' }}" placeholder="nama why choose us" class="form-control">
                  </div>
                </div>
                <div class="form-group lable-padd">
                  <label class="col-sm-3">Deskripsi</label>
                  <div class="col-sm-9">
                    <textarea required class="my-editor form-control" name="description" placeholder="deskripsi" id="my-editor">{{ !empty($why_choose_us->description) ? $why_choose_us->description : '' }}</textarea>
                  </div>
                </div>
                <div class="form-group lable-padd">
                    <label class="col-sm-3">Status</label>
                    <div class="col-sm-9">
                        <select required class="form-control" name="status" id="status">
                          <option value="">-- status --</option>
                          <option value="1" {{ !empty($why_choose_us->status) && $why_choose_us->status == 1 ? 'selected': '' }}>Aktif</option>
                          <option value="0" {{ !empty($why_choose_us->status) && $why_choose_us->status == 0 ? 'selected': '' }}>Non Aktif</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                         <button type="submit" class="btn btn-success col-md-12">
                            Simpan
                         </button>
                    </div>
                </div>
            </form>
         </div>
     </div>
  </div>
</div>
@endsection