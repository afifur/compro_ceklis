@extends('layouts.app_admin')

@section('content')
	<div class="row">
        <div class="col-md-12">
          	<div class="block-web">
	            <div class="header">
	            	<h3 class="content-header">List Why Choose Us</h3>
	            </div>
	            <div class="porlets-content">
	            	<a href="{{ route('create-why-choose-us') }}" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp; Tambah Why Choose Us</a> <br><br>
	            	<table class="table table-hover table-bordered" id="dynamic-table">
	            		<thead>
	            			<tr>
		            			<th>No</th>
		            			<th>Nama</th>
		            			<th>Deskripsi</th>
		            			<th>Status</th>
		            			<th>Action</th>
		            		</tr>
	            		</thead>
	            		<tbody>
	            			@if(count($why_choose_us) > 0)
		            			@foreach($why_choose_us as $why_choose_uss)
			            			<tr>
				            			<td>{{ $loop->iteration }}</td>
				            			<td width="300">{!! $why_choose_uss->title !!}</td>
				            			<td>{!! html_entity_decode($why_choose_uss->description) !!}</td>
				            			<td width="120">
				            				@if($why_choose_uss->status == 1)
				            				<span class="label label-success">Aktif</span>
				            				@else
				            				<span class="label label-danger">Non Aktif</span>
				            				@endif
				            			</td>
				            			<td width="120">
				            				<a href="{{ route('edit-why-choose-us', $why_choose_uss->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>&nbsp;
				            				<a href="{{ route('delete-why-choose-us', $why_choose_uss->id) }}" class="btn btn-danger btn-sm" onclick="return confirm('Ingin menghapus data ini ?')"><i class="fa fa-trash-o"></i></a>
				            			</td>
				            		</tr>
			            		@endforeach
		            		@endif
	            		</tbody>
	            	</table>
	            </div>
	        </div>
	    </div>
	</div>
@endsection