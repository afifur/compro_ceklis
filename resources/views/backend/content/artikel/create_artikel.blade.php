@extends('layouts.app_admin')

@section('content')
<div class="row">
  <div class="col-md-10">
    <div class="block-web">
         <div class="header">
           <h3 class="content-header">
              <a href="{{ route('artikel') }}"><i class="fa fa-arrow-left"></i></a> &nbsp;
              Form Artikel
           </h3>
         </div>
         <div class="porlets-content">
            <form action="{{ route('save-artikel') }}" method="POST" enctype="multipart/form-data" class="form-horizontal row-border">
                @csrf
                <div class="form-group lable-padd">
                    <label class="col-sm-3">Gambar Artikel</label>
                    <div class="col-sm-9">
                       @if(!empty($artikel->image))
                       <img src="{{ url('upload/artikel/'.$artikel->image) }}" width="250" height="250" class="preview_image" id="preview_image" style="object-fit: cover;"><br><br>
                       @else
                       <img src="{{ url('backend/images/image_default.png') }}" width="250" height="250" class="preview_image" id="preview_image"><br><br>
                       @endif
                       <input type="file" name="image" id="image" class="upload">
                    </div>
                </div>
                <div class="form-group lable-padd">
                  <label class="col-sm-3">Kategori Artikel</label>
                  <div class="col-sm-9">
                     <select required class="form-control" name="category_article_id" id="category_article_id">
                        <option value="">-- pilih kategori artikel --</option>
                        @if(count($kategori_artikel) > 0)
                          @foreach($kategori_artikel as $category)
                            <option value="{{ $category->id }}" {{ !empty($artikel->category_article_id) && $artikel->category_article_id == $category->id ? 'selected':'' }} >{{ $category->nama_kategori }}</option>
                          @endforeach
                        @endif
                     </select>
                  </div>
                </div>
                <div class="form-group lable-padd">
                  <label class="col-sm-3">Judul Artikel</label>
                  <div class="col-sm-9">
                     <input type="hidden" name="id" value="{{ !empty($artikel->id) ? $artikel->id : NULL }}">
                     <input required type="text" name="judul" id="judul" value="{{ !empty($artikel->judul) ? $artikel->judul : '' }}" placeholder="judul artikel" class="form-control">
                  </div>
                </div>
                <div class="form-group lable-padd">
                  <label class="col-sm-3">Isi Artikel</label>
                  <div class="col-sm-9">
                    <textarea class="my-editor form-control" name="deskripsi" id="my-editor">{{ !empty($artikel->deskripsi) ? $artikel->deskripsi : '' }}</textarea>
                  </div>
                </div>
                <div class="form-group lable-padd">
                  <label class="col-sm-3">Penulis</label>
                  <div class="col-sm-9">
                      <input required type="text" name="penulis" id="penulis" value="{{ !empty($artikel->penulis) ? $artikel->penulis : '' }}" placeholder="penulis artikel" class="form-control">
                  </div>
                </div>
                <div class="form-group lable-padd">
                  <label class="col-sm-3">Link Referensi</label>
                  <div class="col-sm-9">
                      <input type="text" name="link" id="link" value="{{ !empty($artikel->link) ? $artikel->link : '' }}" placeholder="link" class="form-control">
                  </div>
                </div>
                <div class="form-group lable-padd">
                    <label class="col-sm-3">Status</label>
                    <div class="col-sm-9">
                        <select required class="form-control" name="status" id="status">
                          <option value="">-- status --</option>
                          <option value="1" {{ !empty($artikel->status) && $artikel->status == 1 ? 'selected': '' }}>Aktif</option>
                          <option value="0" {{ !empty($artikel->status) && $artikel->status == 0 ? 'selected': '' }}>Non Aktif</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                         <button type="submit" class="btn btn-success col-md-12">
                            Simpan
                         </button>
                    </div>
                </div>
            </form>
         </div>
     </div>
  </div>
</div>
@endsection

@push('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('my-editor');
    </script>
@endpush