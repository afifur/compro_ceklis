@extends('layouts.app_admin')

@section('content')
	<div class="row">
        <div class="col-md-12">
          	<div class="block-web">
	            <div class="header">
	            	<h3 class="content-header">List What Makes Us Uniq</h3>
	            </div>
	            <div class="porlets-content">
	            	<a href="{{ route('create-what-makes-us-uniq') }}" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp; Tambah What Makes Us Uniq</a> <br><br>
	            	<table class="table table-hover table-bordered" id="dynamic-table">
	            		<thead>
	            			<tr>
		            			<th>No</th>
		            			<th>Nama</th>
		            			<th>Deskripsi</th>
		            			<th>Status</th>
		            			<th>Action</th>
		            		</tr>
	            		</thead>
	            		<tbody>
	            			@if(count($what_makes_us_uniq) > 0)
		            			@foreach($what_makes_us_uniq as $what_makes_us_uniqs)
			            			<tr>
				            			<td>{{ $loop->iteration }}</td>
				            			<td width="300">{!! $what_makes_us_uniqs->title !!}</td>
				            			<td>{!! html_entity_decode($what_makes_us_uniqs->description) !!}</td>
				            			<td width="120">
				            				@if($what_makes_us_uniqs->status == 1)
				            				<span class="label label-success">Aktif</span>
				            				@else
				            				<span class="label label-danger">Non Aktif</span>
				            				@endif
				            			</td>
				            			<td width="120">
				            				<a href="{{ route('edit-what-makes-us-uniq', $what_makes_us_uniqs->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>&nbsp;
				            				<a href="{{ route('delete-what-makes-us-uniq', $what_makes_us_uniqs->id) }}" class="btn btn-danger btn-sm" onclick="return confirm('Ingin menghapus data ini ?')"><i class="fa fa-trash-o"></i></a>
				            			</td>
				            		</tr>
			            		@endforeach
		            		@endif
	            		</tbody>
	            	</table>
	            </div>
	        </div>
	    </div>
	</div>
@endsection