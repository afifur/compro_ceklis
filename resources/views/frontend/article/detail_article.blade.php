@extends('layouts.app_frontend')

@section('content')
<section id="page-content">
	<div class="container-fluid">
		<div class="row">
			<div class="content col-lg-12">
                <div id="blog" class="single-post">
                    <div class="post-item">
                        <div class="post-item-wrap">
                            <div class="post-image">
                                <a href="#">
                                    <img alt="" src="{{ url('upload/artikel/'.$detail->image) }}">
                                </a>
                            </div>
                            <div class="post-item-description">
                                <h2>{{ $detail->judul }}</h2>
                                <div class="post-meta">
                                    <span class="post-meta-date"><i class="fa fa-calendar-o"></i>{!! date('d F Y', strtotime($detail->created_at)) !!}</span>
                                    <span class="post-meta-comments"><a href=""><i class="fa fa-tag"></i> {{ !empty($detail->RefCategory->nama_kategori) ? $detail->RefCategory->nama_kategori : '' }}</a></span>
                                    <span class="post-meta-category"><a href=""><i class="fa fa-user"></i>{{ $detail->penulis }}</a></span>
                                    <div class="post-meta-share">
                                        <a class="btn btn-xs btn-slide btn-facebook" target="_blank" href="http://www.facebook.com/share.php?u={{ URL::to($detail->slug) }}">
                                            <i class="fab fa-facebook-f"></i>
                                            <span>Facebook</span>
                                        </a>
                                        <a class="btn btn-xs btn-slide btn-linkedin" target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url={{ URL::to($detail->slug) }}" data-width="100">
                                            <i class="fab fa-linkedin"></i>
                                            <span>LinkedIn</span>
                                        </a>
                                        <a class="btn btn-xs btn-slide btn-twitter" target="_blank" href="https://twitter.com/share?url={{ URL::to($detail->slug) }}" data-width="118">
                                            <i class="fab fa-twitter"></i>
                                            <span>Twitter</span>
                                        </a>
                                    </div>
                                </div>
                                <p>
                                	{!! $detail->deskripsi !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <h3 class="text-center">Other Articles</h3>
                <br>
				<div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">
					@if(count($other_article) > 0)
						@foreach($other_article as $articles)
							<div class="post-item border">
				                <div class="post-item-wrap">
				                    <div class="post-image" style="height: 230px;">
				                        <a href="#">
				                        	@if(!empty($articles->image))
				                            <img alt="" src="{{ url('upload/artikel/'.$articles->image) }}">
				                            @else
				                            <img alt="" src="{{ url('backend/images/image_default') }}">
				                            @endif
				                        </a>
				                        <span class="post-meta-category"><a href="">{{ !empty($articles->RefCategory->nama_kategori) ? $articles->RefCategory->nama_kategori : '' }}</a></span>
				                    </div>
				                    <div class="post-item-description">
				                        <span class="post-meta-date"><i class="fa fa-calendar-o"></i>{!! date('d F Y', strtotime($articles->created_at)) !!}</span>
				                        <span class="post-meta-comments"><a href=""><i class="fa fa-user"></i>{{ $articles->penulis }}</a></span>
				                        <h2>
				                        	<a href="#">{!! Str::limit($articles->judul, 25, $end='...') !!}</a>
				                        </h2>
				                        <p>{!! Str::limit($articles->deskripsi, 200, $end='...') !!}</p>
				                        <a href="{{ route('detail-article', $articles->slug) }}" class="item-link">Read More <i class="icon-chevron-right"></i></a>
				                    </div>
				                </div>
				            </div>
			        	@endforeach
		            @endif
				</div>
            </div>
		</div>
	</div>
</section>
@endsection