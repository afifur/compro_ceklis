@extends('layouts.app_frontend')

@section('content')
<section id="page-content">
	<div class="container-fluid">
		<h3 class="text-center">New Articles</h3>
		<div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">
			@if(count($article) > 0)
				@foreach($article as $articles)
					<div class="post-item border">
		                <div class="post-item-wrap">
		                    <div class="post-image" style="height: 300px;">
		                        <a href="#">
		                        	@if(!empty($articles->image))
		                            <img alt="" src="{{ url('upload/artikel/'.$articles->image) }}">
		                            @else
		                            <img alt="" src="{{ url('backend/images/image_default') }}">
		                            @endif
		                        </a>
		                        <span class="post-meta-category"><a href="">{{ !empty($articles->RefCategory->nama_kategori) ? $articles->RefCategory->nama_kategori : '' }}</a></span>
		                    </div>
		                    <div class="post-item-description" style="height: 250px;">
		                        <span class="post-meta-date"><i class="fa fa-calendar-o"></i>{!! date('d F Y', strtotime($articles->created_at)) !!}</span>
		                        <span class="post-meta-comments"><a href=""><i class="fa fa-user"></i>{{ $articles->penulis }}</a></span>
		                        <h2>
		                        	<a href="#">{!! Str::limit($articles->judul, 25, $end='...') !!}</a>
		                        </h2>
		                        <p>{!! Str::limit($articles->deskripsi, 200, $end='...') !!}</p>
		                        <a href="{{ route('detail-article', $articles->slug) }}" class="item-link" style="position: absolute; bottom: 0; margin-bottom: 10px; color: #FB6E16;">Read More <i class="icon-arrow-right"></i></a>
		                    </div>
		                </div>
		            </div>
	        	@endforeach
            @endif
		</div>
		<center>
			{{ $article->links() }}
		</center>
	</div>
</section>
@endsection