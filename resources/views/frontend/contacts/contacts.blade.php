@extends('layouts.app_frontend')

@section('content')
<style type="text/css">
	.btn-custom-orange{
		background-color: #FB6E16 !important;
	}

	.description-submit-contact h3 {

	}

	.description-submit-contact p {
		font-size: 13px;
		font-style: normal;
		font-weight: 325;
		color: #3C404F;
	}

	.description-submit-contact-ul {
		font-size: 13px;
		font-style: normal;
		font-weight: 325;
		color: #3C404F;
	}

	.hader-submit h4 {
		font-style: normal;
		font-weight: 400;
		font-size: 18px;
		line-height: 32px;
		color: #0F1014;
	}

	.hader-submit p {
		font-size: 12px;
		font-style: normal;
		font-weight: 325;
		color: #3C404F;
		margin-top: -10px;
	}
</style>
<section id="page-content">
	<div class="container-fluid">
		<h3 class="text-center">Contact Us</h3>
		<br><br>
		<div class="row">
			<div class="col-md-6">
				<div class="description-submit-contact">
					<h3>
						Started from chats, we <br>
						can make something grat
					</h3><br>
					<p>
						Reach out today for a personalized plan, custom<br>
						enterprise options, pricing, and product information<br>
						or just to chat about business plan. Our team is <br>
						happy to help.<br>
					</p>
					<p>
						{!! !empty($contacts->description) ? html_entity_decode($contacts->description) : '' !!}
					</p><br>
					<ul class="list">
	                    <li class="description-submit-contact-ul"><i class="fa fa-phone"></i>&nbsp;&nbsp;{{ !empty($contacts->value1) ? $contacts->value1 : '' }}</li>
	                    <li class="description-submit-contact-ul"><i class="fa fa-envelope"></i>&nbsp;&nbsp;{{ !empty($contacts->value4) ? $contacts->value4 : '' }}</li>
	                </ul>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<div class="card-body">
						<div class="hader-submit">
							<h4>Please Contact Us</h4>
							<p>Fill in the form below</p>
						</div>
						<form action="{{ route('save-contacts') }}" method="POST">
							@csrf
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="text-orange">First Name</label>
										<input required type="text" name="nama_depan" id="nama_depan" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="text-orange">Last Name</label>
										<input type="text" name="nama_belakang" id="nama_belakang" class="form-control">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="text-orange">Email</label>
								<input required type="email" name="email" id="email" class="form-control">
							</div>
							<div class="form-group">
								<label class="text-orange">Contact Number</label>
								<input type="text" name="no_hp" id="no_hp" class="form-control">
							</div>
							<div class="form-group">
								<label class="text-orange">Messages</label>
								<textarea required name="pesan" id="pesan" class="form-control" rows="6"></textarea>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-warning btn-custom-orange col-md-12">
									SUBMIT
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
