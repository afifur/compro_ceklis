@extends('layouts.app_frontend')

@section('content')
<style type="text/css">
	.text-privacy p {
		font-size: 13px;
		font-style: normal;
		font-weight: 325;
		/*font-size: 16px;*/
		line-height: 24px;
		color: #4A5063;
		text-align: justify;
	}
</style>
<section id="page-content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2">
				@include('frontend.sidebar_privacy_term')
			</div>
			<div class="col-md-10">
				<h3 class="text-center">Privacy</h3>
				<div class="text-privacy">
					{!! html_entity_decode($privacy->description) !!}
				</div>
			</div>
		</div>
	</div>
</section>
@endsection