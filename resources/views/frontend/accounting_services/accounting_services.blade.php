@extends('layouts.app_frontend')

@section('content')
<style type="text/css">
	.custom-card{
		box-shadow: 0px 0px 1px rgba(0, 0, 0, 0.32), 0px 4px 18px rgba(0, 0, 0, 0.12);
		border-radius: 16px;
		margin-bottom: 30px;
	}

	.custom-card:hover {
		border: 1px solid #FB6E16;
		box-shadow: 0px 0px 0px 6px rgba(251, 110, 22, 0.16);
	}

	.custom-card:hover .custom-hubungi-kami{
		opacity: 1;
	}

	.custom-hubungi-kami{
		opacity: 0;
		background-color: #FB6E16 !important;
		border-radius: 8px;
		color: #FFFFFF !important;
	}

	.header-accounting-services h3{
		font-style: normal;
		font-weight: 400;
		font-size: 32px;
		line-height: 40px;
		color: #0F1014;
	}

	.header-accounting-services p{
		font-style: normal;
		font-weight: 325;
		font-size: 16px;
		line-height: 24px;
		color: #4A5063;
	}

	.price {
		color: #3C404F;
		font-style: normal;
		font-weight: 350;
		font-size: 20px;
		line-height: 32px;
	}
</style>
<section id="page-content" style="background-image: url('{{ url('frontend/images/background_services.png') }}'); background-size: 100%;">
	<div class="container-fluid">
		<h3 class="text-center">Accounting Services</h3>
		<br><br>
		<div class="row">
			<div class="col-md-4 header-accounting-services">
				<h3>Accounting<br> Services platform</h3>
				<p>Compare Ceklis offerings, plans, and pricing. Get started with Ceklist Accounting platform !</p>
			</div>
			<div class="col-md-8">
				<div class="row">
					@if(count($accounting_services) > 0)
						@foreach($accounting_services as $accounting_services)
							<div class="col-lg-4">
		                        <div class="card custom-card">
		                            <div class="card-body">
		                            	<div align="center">
		                            		<img src="{{ url('upload/accounting-services/'.$accounting_services->image) }}" width="30" height="30">
		                            	</div>
		                            	<br>
		                                <h5 class="card-title text-center" style="height: 70px; font-size: 16px;">{{ $accounting_services->title }}</h5><br>
		                                <h6 class="card-subtitle mb-2 text-muted" style="padding-left: 15px; height: 180px;">
		                                	{!! html_entity_decode($accounting_services->description) !!}
		                                </h6>
		                                @if($accounting_services->value1 == 0)
		                                <div style="height: 80px;">
		                                	<h4 class="price text-center">-</h4>
		                                </div>
		                                @else
		                                <div style="height: 80px;">
		                                	<h4 class="price">IDR {!! number_format($accounting_services->value1) !!}<span style="font-size: 9px; color: #3C404F;">/bln</span></h4>
		                                </div>
		                                @endif
		                                <a href="https://api.whatsapp.com/send?phone={{ !empty($contacts->value3) ? $contacts->value3 : '' }}" class="btn btn-light col-lg-12 custom-hubungi-kami">Hubungi Kami</a>
		                            </div>
		                        </div>
		                    </div>
	                    @endforeach
                    @endif
				</div>
			</div>
		</div>
	</div>
</section>
@endsection