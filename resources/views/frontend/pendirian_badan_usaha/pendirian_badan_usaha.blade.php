@extends('layouts.app_frontend')

@section('content')
<style type="text/css">
	.table-curved {
    	border-collapse: separate;
	}
	.table-curved {
	    border: solid #ccc 0px;
	    border-radius: 6px;
	    border-left:0px;
	}
	.table-curved td, .table-curved th {
	    border-left: 0px solid #ccc;
	    border-top: 1px solid #ccc;
	}
	.table-curved th {
	    border-top: none;
	}
	.table-curved th:first-child {
	    border-radius: 10px 0 0 0;
	}
	.table-curved th:last-child {
	    border-radius: 0 10px 0 0;
	}
	.table-curved th:only-child{
	    border-radius: 10px 10px 0 0;
	}
	.table-curved tr:last-child td:first-child {
	    border-radius: 0 0 0 10px;
	}
	.table-curved tr:last-child td:last-child {
	    border-radius: 0 0 10px 0;
	}

	.list-custom li {
		height: 48.5px;
	}
</style>
<section id="page-content">
	<div class="container-fluid">
		<h3 class="text-center">Pendirian Badan Usaha</h3>
		<br><br>
		@if(count($pendirian_badan_usaha) > 0)
			@foreach($pendirian_badan_usaha as $value)
				<div class="row">
					<div class="col-md-4">
						<div style="margin-top: 10px;">
							<h4>{{ $value->title }}</h4>
							<ul class="list list-custom">
								<li>IDR</li>
								<li>Konsultasi Gratis</li>
								<li>Akta Perusahaan</li>
								<li>SK Menkuham</li>
								<li>NPWP & SKT</li>
								<li>SIUP</li>
								<li>NIB TDP & API</li>
								<li>Virtual Office 1 th</li>
							</ul>
						</div>
					</div>
					<div class="col-md-8">
						<table class="table table-curved">
							<thead>
								<tr style="background-color: #4F896F; color: #FFFFFF;">
									<th class="text-center">LITE</th>
									<th class="text-center">FULL</th>
									<th class="text-center">FULL +</th>
								</tr>
							</thead>
							@php
	                            $dtl = "";
	                            if(!empty($value->description)){
	                              $dtl = json_decode($value->description, TRUE);
	                            }
	                            
	                       	@endphp
							<tbody>
								<tr>
									<td align="center" class="text-warning">IDR {{ !empty($dtl['IDR']['idr_lite']) ? number_format($dtl['IDR']['idr_lite']) : 0 }}</td>
									<td align="center" class="text-warning">IDR {{ !empty($dtl['IDR']['idr_full']) ? number_format($dtl['IDR']['idr_full']) : 0 }}</td>
									<td align="center" class="text-warning">IDR {{ !empty($dtl['IDR']['idr_plus']) ? number_format($dtl['IDR']['idr_plus']) : 0 }}</td>
								</tr>
								<tr>
									<td align="center" class="text-success">
										@if(!empty($dtl['konsultasi_gratis']['konsultasi_gratis_lite']) && $dtl['konsultasi_gratis']['konsultasi_gratis_lite'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
									<td align="center" class="text-success">
										@if(!empty($dtl['konsultasi_gratis']['konsultasi_gratis_full']) && $dtl['konsultasi_gratis']['konsultasi_gratis_full'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
									<td align="center" class="text-success">
										@if(!empty($dtl['konsultasi_gratis']['konsultasi_gratis_plus']) && $dtl['konsultasi_gratis']['konsultasi_gratis_plus'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
								</tr>
								<tr>
									<td align="center" class="text-success">
										@if(!empty($dtl['akta_perusahaan']['akta_perusahaan_lite']) && $dtl['akta_perusahaan']['akta_perusahaan_lite'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
									<td align="center" class="text-success">
										@if(!empty($dtl['akta_perusahaan']['akta_perusahaan_full']) && $dtl['akta_perusahaan']['akta_perusahaan_full'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
									<td align="center" class="text-success">
										@if(!empty($dtl['akta_perusahaan']['akta_perusahaan_plus']) && $dtl['akta_perusahaan']['akta_perusahaan_plus'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
								</tr>
								<tr>
									<td align="center" class="text-success">
										@if(!empty($dtl['sk_menkuham']['sk_menkuham_lite']) && $dtl['sk_menkuham']['sk_menkuham_lite'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
									<td align="center" class="text-success">
										@if(!empty($dtl['sk_menkuham']['sk_menkuham_full']) && $dtl['sk_menkuham']['sk_menkuham_full'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
									<td align="center" class="text-success">
										@if(!empty($dtl['sk_menkuham']['sk_menkuham_plus']) && $dtl['sk_menkuham']['sk_menkuham_plus'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
								</tr>
								<tr>
									<td align="center" class="text-success">
										@if(!empty($dtl['npwp_skt']['npwp_skt_lite']) && $dtl['npwp_skt']['npwp_skt_lite'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
									<td align="center" class="text-success">
										@if(!empty($dtl['npwp_skt']['npwp_skt_full']) && $dtl['npwp_skt']['npwp_skt_full'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
									<td align="center" class="text-success">
										@if(!empty($dtl['npwp_skt']['npwp_skt_plus']) && $dtl['npwp_skt']['npwp_skt_plus'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
								</tr>
								<tr>
									<td align="center" class="text-success">
										@if(!empty($dtl['siup']['siup_lite']) && $dtl['siup']['siup_lite'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
									<td align="center" class="text-success">
										@if(!empty($dtl['siup']['siup_full']) && $dtl['siup']['siup_full'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
									<td align="center" class="text-success">
										@if(!empty($dtl['siup']['siup_plus']) && $dtl['siup']['siup_plus'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
								</tr>
								<tr>
									<td align="center" class="text-success">
										@if(!empty($dtl['nib_tdp_api']['nib_tdp_api_lite']) && $dtl['nib_tdp_api']['nib_tdp_api_lite'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
									<td align="center" class="text-success">
										@if(!empty($dtl['nib_tdp_api']['nib_tdp_api_full']) && $dtl['nib_tdp_api']['nib_tdp_api_full'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
									<td align="center" class="text-success">
										@if(!empty($dtl['nib_tdp_api']['nib_tdp_api_plus']) && $dtl['nib_tdp_api']['nib_tdp_api_plus'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
								</tr>
								<tr>
									<td align="center" class="text-success">
										@if(!empty($dtl['virtual_office']['virtual_office_lite']) && $dtl['virtual_office']['virtual_office_lite'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
									<td align="center" class="text-success">
										@if(!empty($dtl['virtual_office']['virtual_office_full']) && $dtl['virtual_office']['virtual_office_full'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
									<td align="center" class="text-success">
										@if(!empty($dtl['virtual_office']['virtual_office_plus']) && $dtl['virtual_office']['virtual_office_plus'] == "YA")
											<i class="fa fa-check"></i>
										@else
											<i class="fa fa-minus"></i>
										@endif
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<br><br><br><br>
			@endforeach
		@endif
	</div>
</section>
@endsection