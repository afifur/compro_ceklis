@extends('layouts.app_frontend')

@section('content')
<style type="text/css">
	.shadow-radius{
		box-shadow: 0px 0px 1px rgba(0, 0, 0, 0.32), 0px 4px 18px rgba(0, 0, 0, 0.12);
		border-radius: 16px !important;
	}

	.header-timelines{
		font-size: 18px;
	}

	.text-timeline{
		font-size: 13px;
		font-style: normal;
		font-weight: 325;
		/*font-size: 16px;*/
		line-height: 24px;
		color: #4A5063;
		text-align: justify;
	}

	.banner-about-us{
		margin-top: 100px;
		position: relative;
	}

	.text-about-us{
		position: absolute;
	    top: 50%;
	    left: 50%;
	    width: 100%;
	    transform: translate(-50%, -50%);
	}

	.banner-width {
		width: 100%;
	}

	.team-desc h3 {
		font-style: normal;
		font-weight: 350;
		font-size: 16px;
		line-height: 24px;
		color: #3C404F;
	}

	.team-desc p {
		font-size: 12px;
		font-style: normal;
		font-weight: 325;
		line-height: 0px;
		color: #3C404F;
		margin-bottom: 30px;
	}

	.team-desc span {

	}

	.subheader-who-we-are {
		font-style: normal;
		font-weight: 325;
	}

	@media only screen and (max-device-width: 480px) {
        .banner-width {
            width: 100%;
            height: 300px;
        }

        .text-banner {
        	padding: 20px;
        }
    }

    .text-linkedin {
    	font-weight: 350;
		font-size: 14px;
		line-height: 22px;
		color: #FFFFFF;
		text-transform: capitalize;
    }
</style>
<section id="page-content">
	<div class="container-fluid">
		<h3 class="text-center">Our Team</h3>
		<p class="text-center">Meet our team member, The people who work at Distruptive share the vision and value of our community</p>
		<br><br>
		<div class="row team-members m-b-40">
			@if(count($team) > 0)
				@foreach($team as $teams)
				<div class="col-md-3">
	                <div class="team-member shadow-radius text-center">
	                    <div class="team-image">
	                        <img src="{{ url('upload/team/'.$teams->image) }}">
	                    </div>
	                    <div class="team-desc">
	                        <h3>{{ $teams->nama }}</h3>
	                        <p>{{ $teams->jabatan }}</p>
	                        <a class="btn btn-sm btn-linkedin" href="{{ $teams->link_linkedin }}" target="_blank" style="width: 100%;">
                                <span class="text-linkedin">Linked<img src="{{ url('frontend/images/linkedin.png') }}" style="margin-top: -3px;"></span>
                            </a>
	                    </div>
	                </div>
	            </div>
	            @endforeach
            @endif
		</div>

		<h3 class="text-center">Who We Are?</h3>
		<p class="text-center subheader-who-we-are">The people who work at Distruptive share the vision and value of our community</p>
		<br><br>
		<div class="row">
			<div class="col-md-5" style="margin-bottom: 30px;">
				@if(!empty($about_us->image))
				<img src="{{ url('upload/about-us/'.$about_us->image) }}" width="100%">
				@endif
			</div>
			<div class="col-md-7">
				<ul class="timeline">
                    <li class="timeline-item">
                        <div class="timeline-icon"><i class="icon-box" style="margin-top: 3px;"></i></div>
                        <h5 class="header-timelines">10 Tahun bergerak di bidang keuangan dan perpajakan</h5>
                        <p class="text-timeline">
                        	Didirikan oleh praktisi di bidang Akuntansi, Keuangan dan Perpajakan, founder yang berpengalaman sebagai seorang Auditor Eksternal di Kantor Akuntan Publik yang berpengalaman menangani klien berskala nasional dan internasional dan juga sebagai Akuntan Corporate di perusahaan multinasional.
                        </p>
                    </li>
                    <li class="timeline-item">
                        <div class="timeline-icon"><i class="icon-life-buoy" style="margin-top: 3px;"></i></div>
                        <h5 class="header-timelines">Consulting & Company</h5>
                        <p class="text-timeline">
                        	Tahun 2018 kami resmi terbentuk menjadi konsultan di bidang Akuntansi, Keuangan dan Perpajakan serta jasa umum terkait Ijin Pendirian Usaha. Dengan kompetensi & Integritas, kini dalam 3 tahun kami sudah di percaya untuk menangani berbagai jenis klien pada beranekaragam jenis industri. Kami hadir sebagai solusi untuk membantu para pengusaha membuat laporan keuangan yang baik sesuai dengan Standar Akuntansi Keuangan dan Peraturan Perpajakan yang berlaku di Indonesia dengan harga yang terjangkau. Ceklis.id juga tempat berbagi ide, pengetahuan dan wawasan untuk bertumbuh dan berkembang bersama.
                        </p>
                    </li>
                    <li class="timeline-item">
                        <div class="timeline-icon"><i class="icon-heart" style="margin-top: 3px;"></i></div>
                        <h5 class="header-timelines">Achievment</h5>
                        <p class="text-timeline">
                        	Melalui kolaborasi antara pihak klien dan pihak Ceklist.id, kami telah membantu banyak UMKM dalam menyusun laporan keuangan sesuai Standar Akuntansi Keuangan dan pelaporan perpajakan tepat waktu, selain itu kami juga memberikan masukan atas kinerja keuangan perusahaan serta akses untuk mendapatkan pembiayaan sehingga para pelaku usaha dapat meningkatkan kapasitas usahanya.
                        </p>
                    </li>
                    <li class="timeline-item">
                        <div class="timeline-icon"><i class="icon-shield" style="margin-top: 3px;"></i></div>
                        <h5 class="header-timelines">Our Speciality</h5>
                        <p class="text-timeline">
                        	Kami sudah memiliki skill dan pengalaman untuk berbagai bidang usaha diantaranya perkebunan, perbankan, pertambangan, manufaktur, retail, perusahaan jasa, FnB dll. Sehingga anda dan kami akan menemukan beberapa kesamaan dan keselarasan untuk berkolaborasi menyajikan laporan keuangan yang andal.
                        </p>
                    </li>
                </ul>
			</div>
		</div>
	</div>
	<div class="banner-about-us">
		<img src="{{ url('frontend/images/banner_about_us.png') }}" class="banner-width" style="object-fit: cover;">
		<div class="text-about-us text-light text-center text-banner">
			<h3 class="text-center">Choose your plane with Ceklis.id</h3>
			<p class="text-center">Build for free and purchase your Teams plan at any time, or reach out for Ceklis.id and the Enterprise Build Platform</p>
			<a href="{{ route('contacts') }}" class="btn btn-warning">Hubungi Kami</a>
		</div>
	</div>
	
</section>
@endsection