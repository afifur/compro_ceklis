@extends('layouts.app_frontend')

@section('content')
<style type="text/css">
	.banner-about-us{
		margin-top: 100px;
		position: relative;
		margin-bottom: -40px;
	}

	.banner-width {
		width: 100%;
	}

	@media only screen and (max-device-width: 480px) {
        .banner-width {
            width: 100%;
            height: 300px;
        }

        .text-banner {
        	padding: 20px;
        }
    }

    .text-about-us{
		position: absolute;
	    top: 50%;
	    left: 50%;
	    width: 100%;
	    transform: translate(-50%, -50%);
	}
</style>
<section id="page-content" style="background-image: url('{{ url('frontend/images/background_services.png') }}'); background-size: 100%;">
	<div class="container-fluid">
		<h3 class="text-center">Finance Services</h3>
		<br><br>
		<div class="row">
			<div class="col-md-4">
				<img src="{{ url('frontend/images/finance_services.png') }}" width="100%">
			</div>
			<div class="col-md-8">
				<ul class="list-icon list-icon-circle list-icon-colored red text-justify">
					<div class="row">
						@if(count($finance_services) > 0)
							@foreach($finance_services as $finance_services)
							<div class="col-md-6" style="margin-bottom: 50px;">
								<li>
									<div class="header-why-choose-us">{{ $finance_services->title }}</div>
									<div class="desc-why-choose-us">{!! html_entity_decode($finance_services->description) !!}</div>
								</li>
							</div>
							@endforeach
                    	@endif
					</div>
					
				</ul>
			</div>
		</div>
	</div>
	<div class="banner-about-us">
		<img src="{{ url('frontend/images/banner_about_us.png') }}" class="banner-width" style="object-fit: cover;">
		<div class="text-about-us text-light text-center text-banner">
			<h3 class="text-center">Choose your plane with Ceklis.id</h3>
			<p class="text-center">Build for free and purchase your Teams plan at any time, or reach out for Ceklis.id and the Enterprise Build Platform</p>
			<a href="{{ route('contacts') }}" class="btn btn-warning">Hubungi Kami</a>
		</div>
	</div>
</section>
@endsection